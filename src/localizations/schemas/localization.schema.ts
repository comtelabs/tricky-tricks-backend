import { Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class Localization extends Document {
    @ApiProperty({ type: String })
    @Prop()
    city: string;

    @ApiProperty({ type: String })
    @Prop()
    region: string;

    @ApiProperty({ type: String })
    @Prop()
    country: string;
}
