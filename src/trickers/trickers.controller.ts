import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
} from '@nestjs/common';
import { TrickersService } from './trickers.service';
import { CreateTrickerDto } from './dto/create-tricker.dto';
import { UpdateTrickerDto } from './dto/update-tricker.dto';

@Controller('trickers')
export class TrickersController {
    constructor(private trickersService: TrickersService) {}

    @Post()
    create(@Body() createTrickerDto: CreateTrickerDto) {
        return this.trickersService.create(createTrickerDto);
    }

    @Get()
    findAll() {
        return this.trickersService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.trickersService.findOne(id).catch();
    }

    @Patch(':id')
    update(
        @Param('id') id: string,
        @Body() updateTrickerDto: UpdateTrickerDto,
    ) {
        return this.trickersService.update(id, updateTrickerDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.trickersService.remove(id);
    }
}
