import { Test, TestingModule } from '@nestjs/testing';
import { TrickersController } from './trickers.controller';
import { TrickersService } from './trickers.service';

describe('TrickersController', () => {
    let controller: TrickersController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TrickersController],
            providers: [TrickersService],
        }).compile();

        controller = module.get<TrickersController>(TrickersController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
