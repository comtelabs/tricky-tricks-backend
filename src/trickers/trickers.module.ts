import { Module } from '@nestjs/common';
import { TrickersService } from './trickers.service';
import { TrickersController } from './trickers.controller';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Tricker, TrickerSchema } from './schemas/tricker.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Tricker.name, schema: TrickerSchema },
        ]),
    ],
    controllers: [TrickersController],
    providers: [TrickersService],
})
export class TrickersModule {}
