import { Test, TestingModule } from '@nestjs/testing';
import { TrickersService } from './trickers.service';

describe('TrickersService', () => {
    let service: TrickersService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TrickersService],
        }).compile();

        service = module.get<TrickersService>(TrickersService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
