export enum TrickerRole {
    Admin = 'Admin',
    Moderator = 'Moderator',
    User = 'User',
}
