import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude } from 'class-transformer';
import { Document } from 'mongoose';
import { Localization } from '../../localizations/schemas/localization.schema';
import { TrickerRole } from './tricker.role';

export type TrickerDocument = Tricker & Document;

@Schema()
export class Tricker {
    @Prop({ required: true, unique: true })
    nickname: string;

    @Prop({ required: true, unique: true })
    email: string;

    @Prop({ required: true })
    @Exclude()
    password: string;

    @Prop()
    youtube: string;

    @Prop()
    avatar: string;

    @Prop({ type: Localization })
    location: Localization;

    @Prop({ required: true, default: TrickerRole.User })
    @Exclude()
    role: TrickerRole;

    @Prop()
    @Exclude()
    token: string;

    @Prop({ required: true, default: false })
    is_banned: boolean;

    @Prop({ type: Date, default: Date.now })
    created_date: Date;
}

export const TrickerSchema = SchemaFactory.createForClass(Tricker);
