import { Model, ObjectId } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Tricker, TrickerDocument } from './schemas/tricker.schema';
import { CreateTrickerDto } from './dto/create-tricker.dto';
import { UpdateTrickerDto } from './dto/update-tricker.dto';
import * as mongoose from 'mongoose';

@Injectable()
export class TrickersService {
    constructor(
        @InjectModel(Tricker.name) private trickerModel: Model<TrickerDocument>,
    ) {}

    async create(createTrickerDto: CreateTrickerDto): Promise<Tricker> {
        const createdTricker = new this.trickerModel(createTrickerDto);
        return await createdTricker.save();
    }

    async findAll(): Promise<Tricker[]> {
        return this.trickerModel.find().exec();
    }

    async findOne(id: string): Promise<Tricker> {
        return await this.trickerModel.findById(id).exec();
    }

    async update(
        id: string,
        updateTrickerDto: UpdateTrickerDto,
    ): Promise<Tricker> {
        return await this.trickerModel
            .findByIdAndUpdate(id, updateTrickerDto)
            .exec();
    }

    async remove(id: string): Promise<Tricker> {
        return await this.trickerModel.findByIdAndDelete(id).exec();
    }
}
