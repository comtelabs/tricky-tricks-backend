import { Localization } from '../../localizations/schemas/localization.schema';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MaxLength } from 'class-validator';

export class CreateTrickerDto {
    @ApiProperty({
        type: String,
        description: 'a pretty name for a tricky tricker !',
    })
    @IsNotEmpty()
    @MaxLength(64)
    nickname: string;

    @ApiProperty({ type: String, description: 'must be a verified email !' })
    @IsEmail()
    email: string;

    @ApiProperty({ type: String, description: 'a strong password' })
    @IsNotEmpty()
    password: string;

    @ApiProperty({ type: String, description: 'YouTube channel url' })
    youtube: string;

    @ApiProperty({
        type: String,
        description: 'path to the avatar image to load',
    })
    avatar: string;

    @ApiProperty({ type: Localization })
    location: Localization;
}
