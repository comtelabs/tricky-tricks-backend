import { PartialType } from '@nestjs/mapped-types';
import { CreateTrickerDto } from './create-tricker.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
import { TrickerRole } from '../schemas/tricker.role';
import { Localization } from '../../localizations/schemas/localization.schema';

export class UpdateTrickerDto extends PartialType(CreateTrickerDto) {
    @ApiProperty({
        type: String,
        description: 'a pretty name for a tricky tricker !',
    })
    @IsNotEmpty()
    nickname: string;

    @ApiProperty({ type: String, description: 'must be a verified email !' })
    @IsEmail()
    email: string;

    @ApiProperty({ type: String, description: 'YouTube channel url' })
    youtube: string;

    @ApiProperty({
        type: String,
        description: 'path to the avatar image to load',
    })
    avatar: string;

    @ApiProperty({ enum: TrickerRole, enumName: 'TrickerRole' })
    role: TrickerRole;

    @ApiProperty({ type: Localization })
    location: Localization;
}
