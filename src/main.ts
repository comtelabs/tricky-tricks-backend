import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const configService = app.get(ConfigService);

    const config = new DocumentBuilder()
        .setTitle('Tricky Tricks API')
        .setDescription('Swag across Tricky Tricks routes !')
        .setVersion('1.0')
        .addTag('tricky-tricks', 'tricky tricks')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    app.useGlobalPipes(new ValidationPipe());
    await app.listen(configService.get('API_PORT') || 4000);
}
bootstrap();
